#ifndef _MENU_H
#define _MENU_H


#define MAKE_MENU(Name, Next, Child, Select,Enter, variable_display) \
 const menuItem Next; \
 const menuItem  Child;\ 
const menuItem Name = {(void*)&Next, (void*)&Child, (FuncPtr) Select, (FuncPtr)  Enter, variable_display }

#define NULL_ENTRY Null_Menu
#define NULL_FUNC  (void)0
#define NULL_TEXT  0x00
#define NEXT       ((menuItem*)(selectedMenuItem->Next))
#define CHILD      ((menuItem*)(selectedMenuItem->Child))
#define SELECTFUNC   ((FuncPtr)(selectedMenuItem->SelectFunc))
#define ENTERFUNC   ((FuncPtr)(selectedMenuItem->EnterFunc))   

 typedef void (*FuncPtr)(void);
typedef struct
{
   void      *Next;
   void      *Child;
   FuncPtr     SelectFunc;   // ��������� ��� ����������� � ����� ����
   FuncPtr     EnterFunc;   // ��������� ������, ���������� ��� ������� Enter � ����
   int   variable_display; /*���������� ���������*/
} menuItem;


volatile menuItem* selectedMenuItem; // ������� ����� ����
//const menuItem   Null_Menu = {(void*)0, (void*)0, NULL_FUNC, NULL_FUNC, 0};
/*������� "��������� ����"*/
MAKE_MENU (MenuLvl2i1,  MenuLvl2i2,  MenuLvl3i1,  NULL_FUNC,  NULL_FUNC,     0x5E777879);/*DAtE*/
MAKE_MENU (MenuLvl3i1,  MenuLvl3i2,  NULL_ENTRY,  NULL_FUNC,  NULL_FUNC,   0x6d6e7978);/*S ��* ��������� ����*/
#endif